# Setup Your Linux Environment

Bored to set up your Linux env each time you have to reinstall it ? Launch this script :)  

This script allows you to prepare with a single command line your Linux environment.  
The main software you need are installed thanks to this script.

# How to use it?

Launch this command line to execute the script :
```
./setup.sh
```

If you need help to see all available options, you can launch this command line :
```
./setup.sh --help
```

You can add some options to disable one or multiple installations :
```
--no-curl   - to disable Curl install

--no-nvm    - to disable NVM install

--no-java   - to disable Java install

--no-git    - to disable Git install (but who do this?)

--no-yarn   - to disable Yarn install

--no-pipenv - to disable Pipenv install

--no-docker - to disable Docker install

--no-vim    - to disable Vim install
```

Examples :

```
./setup.sh --no-java # install all software except Java

./setup.sh --no-java --no-pipenv # install all software except Java and Pipenv
```