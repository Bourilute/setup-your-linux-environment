#!/bin/bash

#htop
#ngrok
#ohmyzsh
#docker-compose
#react-native-debugger
#android studio

installCurl=true
installNVM=true
installJava=true
installGit=true
installYarn=true
installPipenv=true
intallDocker=true
installVim=true
help=false

function disableAll
{
	installCurl=false
	installNVM=false
	installJava=false
	installGit=false
	installYarn=false
	installPipenv=false
	installDocker=false
	installVim=false
	help=true
}

function disableInstall
{
	case $1 in
	--help)
		echo "Type script.sh with the following options"
		echo ""
		echo "--no-curl     - to disable Curl"
		echo "--no-nvm      - to disable NVM"
		echo "--no-java     - to disable Java"
		echo "--no-git      - to disable Git"
		echo "--no-yarn     - to disable Yarn"
		echo "--no-pipenv   - to disable Pipenv"
		echo "--no-docker   - to disable Docker"
		echo "--no-vim      - to disable Vim"
		echo ""
		disableAll
		;;
	--no-curl)
		# echo "No CURL"
		installCurl=false
		;;
	--no-nvm)
		# echo "No NVM"
		installNVM=false
		;;
	--no-java)
		# echo "No JAVA"
		installJava=false
		;;
	--no-git)
		# echo "No Git"
		installGit=false
		;;
	--no-yarn)
		# echo "No Yarn"
		installYarn=false
		;;
	--no-pipenv)
		# echo "No Pipenv"
		installPipenv=false
		;;
	--no-docker)
		# echo "No Docker"
		installDocker=false
		;;
	--no-vim)
		# echo "No Vim"
		installVim=false
	esac
}

function updateTheComputer
{
	echo '#######################################'
	echo '#       Updating your computer        #'
	echo '#######################################'

	sudo apt update
	sudo apt upgrade
}

function makeTheInstall
{
	if [ $installCurl = true ]
	then
		echo '########################################'
		echo '#           Installing Curl            #'
		echo '########################################'

		sudo apt install curl
	fi

	if [ $installNVM = true ]
	then
		echo '########################################'
		echo '#           Installing NVM             #'
		echo '########################################'

		curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash
		nvm install --lts
		nvm use --lts
	fi

	if [ $installJava = true ]
	then
		echo '########################################'
		echo '#           Installing Java            #'
		echo '########################################'

		sudo add-apt-repository ppa:openjdk-r/ppa
		sudo apt update

		sudo apt install openjdk-8-jdk
		sudo update-alternatives --config java
	fi

	if [ $installGit = true ]
	then
		echo '########################################'
		echo '#           Installing Git             #'
		echo '########################################'

		sudo apt install git
	fi

	if [ $installYarn = true ]
	then
		echo '#######################################'
		echo '#          Installing Yarn            #'
		echo '#######################################'

		sudo curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
		sudo echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
		sudo apt update

		sudo apt install --no-install-recommends yarn
	fi

	if [ $installPipenv = true ]
	then
		echo '#######################################'
		echo '#        Installing Pipenv            #'
		echo '#######################################'

		sudo apt install python3-pip
		sudo -H python3 -m pip install -U pipenv
	fi

	if [ $installDocker = true ]
	then
		echo '#######################################'
		echo '#        Installing Docker            #'
		echo '#######################################'

		sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
		sudo curl -fsSl https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
		sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
		sudo apt update

		sudo apt install docker-ce docker-ce-cli containerd.io
		sudo groupadd docker
		sudo usermod -aG docker $USER
		newgrp docker
		docker run hello-world
	fi

	if [ $installVim = true ]
	then
		echo '#######################################'
		echo '#          Installing Vim             #'
		echo '#######################################'

		sudo apt install vim
		sudo apt update
	fi
}

counter=0
numberOfParameters=$#

while [ $counter -lt $numberOfParameters ]
do
	disableInstall $1
	shift
	let "counter += 1"
done

if [ $help != true ]
then
	updateTheComputer
	makeTheInstall
fi